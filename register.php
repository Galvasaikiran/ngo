<?php

session_start();

include("config.php");

// Connect to server and select databse.
$conn=mysqli_connect($host,$username,$password,$db_name);
if($conn->connect_error){
	die("Connection Error: ". $conn->connect_error);
}

if(isset($_POST['submit'])){
	
	$email = $_POST['email'];
	$firstname = $_POST['fname'];
	$lastname = $_POST['lname'];
	$middlename = $_POST['mname'];
	$password = $_POST['passwd'];
	$countrycode = $_POST['ccode'];
	$phoneno = $_POST['phno'];
	$account_type = $_POST['optradio'];

	$today = getdate();
	$year = $today['year'];

	$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
	$user_id = $year . $salt1;													// Creating user profile id
	
	// Hashing Password
	
	$salt2 = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	$saltedpasswd = $salt2 . $password;
	$hashpassword = hash('sha256', $saltedpasswd);

	$sql1 = "INSERT INTO user_profile VALUES ('$user_id', '$firstname', '$middlename', '$lastname', '$email', '$countrycode', '$phoneno')";
	$sql2 = "INSERT INTO user_account VALUES ('$user_id', '$email', '$hashpassword', '$salt2', 'sha256', 1, '$account_type', 'ACTIVE','N')";
	
	if($conn->query($sql1)==true && $conn->query($sql2)==true){
		if($account_type == "ngo")
		{
			$_SESSION["user_id"] = $user_id;
			header("location:ngosignupform.php");
		}
		else
		{
			$_SESSION['message'] = array("Successfully Registered. Please log in to continue"); 
			header("location:loginhome.php");
		}
	}else{
		$_SESSION['error'] = array("Could not complete registration");
		header("location:loginhome.php");
	}
}
?>