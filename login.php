<?php

session_start();

include("config.php");

// Connect to server and select database.
$conn = mysqli_connect($host,$username,$password,$db_name);
if($conn->connect_error){
	die("Connection Error: ". $conn->connect_error);
}

if(isset($_POST['submit'])){

	$username = $_POST['username'];
	$password = $_POST['password'];

	$sql = "Select * FROM user_account WHERE email = '$username'";
	$result = mysqli_query($conn, $sql);


	if(mysqli_num_rows($result) > 0){
		while($row = mysqli_fetch_assoc($result)){
			$pass = $row["pswd"];
			$salt = $row["pswd_salt"];
			$user_id = $row["user_profile_id"];
			$user_type = $row["user_type"];
		}
	}else{
		$_SESSION['error'] = array("Login Failed : No such Account Exists");
		header("location:loginhome.php");
	}
	
	// Calculating Hash value of Password

	$SaltedPass = $salt . $password;
	$HashedPass = hash('sha256', $SaltedPass);
	
	// Checking If the calculated hash and stored hash are same or not

	if($HashedPass == $pass){
		echo '<script>alert("Login Successful")</script>';
		$_SESSION["user_id"] = $user_id;
		$_SESSION["user_type"] = $user_type;
		if($user_type == "admin")
		{
			$_SESSION["username"] = $username;
			header("location:admin.php");
		}
		
		else if($user_type == "ngo")
	{
		header("location:ngohome.php");
	}
	else{
		$_SESSION['error'] = array("Invalid Login Credentials");
		
	}
	}
	
}

?>
