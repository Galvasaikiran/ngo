<?php
   
      include("login.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/jquery.js"></script>

	<script type="text/javascript">
           function Validateform(){
		      
			  var email = document.getElementById('email').value;
			  var fname = document.getElementById('fname').value;
			  var mname = document.getElementById('mname').value;
			  var lname = document.getElementById('lname').value;
			  var password = document.getElementById('passwd').value;
			  var countrycode = document.getElementById('ccode').value;
			  var phno = document.getElementById('phno').value;
			  var user = document.getElementById('user').checked;
			  var ngo = document.getElementById('ngo').checked;
			  

			  if(email == "" || email == "Email Address"){
			     alert("Email Not Entered");
				 return false;
			  }
			  else if(fname == "" || fname == "First Name"){
				alert("First Name Not Entered");
				return false;
			  }
			  else if(mname == "" || mname == "Middle Name"){
				alert("Middle Name Not Entered");
				return false;
			  }
			  else if(lname == "" || lname == "Last Name"){
				alert("Last Name Not Entered");
				return false;
			  }
			  else if(password == "" || password == "Password"){
			     alert("Password Not Entered");
				 return false;
			  }
			  else if(countrycode == "" || countrycode == "Country Code"){
			     alert("Country Code Not Entered");
				 return false;
			  }else if(phno == "" || phno == "Phone Number"){
			     alert("Phone Number Not Entered");
				 return false;
			  }  
			  else if(user == "" && ngo == ""){
				  alert("Please select account type");
			  }
			  else{
			     return true;
			  }
		   
		   }
		   
	</script>
	<script type="text/javascript">
		   function loginvalidate(){
		   
		   
				var emailid = document.getElementById('login-username').value;
				var passwd = document.getElementById('login-password').value;
				
				if(emailid == "" || emailid == "Email Address"){
					alert("Email Not Entered");
					return false;
				}else if(passwd == "" || passwd == "Password"){
					alert("Password Not Entered");
					return false;
				}else{
					return true;
				}
		   }
        </script>
	
  </head>
  <body>
  <br>
  
  <?php if (isset($_SESSION["error"])): ?>
    <div class="form-errors">
        <?php foreach($_SESSION["error"] as $error): ?>
            <h4 align="center" style="color:red" ><?php echo $error ?></h4>
        <?php endforeach; session_unset(); session_destroy(); ?>
    </div>
  <?php endif; ?> 
  
  <?php if (isset($_SESSION["message"])): ?>
    <div class="form-errors">
        <?php foreach($_SESSION["message"] as $msg): ?>
            <h4 align="center" style="color:red" ><?php echo $msg ?></h4>
        <?php endforeach; session_unset(); Session_destroy(); ?>
    </div>
  <?php endif; ?> 

   <div class="container">    
        <div id="loginbox" style="margin-top:30px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="index.html">Home</a></div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginform" class="form-horizontal" role="form" method="POST" action="login.php" onsubmit="return loginvalidate()">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="login-username" type="email" class="form-control" name="username" placeholder="Email Address">                                        
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    

                                
                            <div class="input-group">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
                                    </div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <button id="btn-login" type="submit" name="submit" class="btn btn-success"> Login  </button>
									  <!--<a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>-->
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Don't have an account! 
                                        <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>    
                            </form>     



                        </div>                     
                    </div>  
        </div>
        <div id="signupbox" style="display:none; margin-top:30px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                        </div>  
                        <div class="panel-body" >
                            <form id="signupform" class="form-horizontal" role="form" method="POST" action="register.php" onsubmit="return Validateform()">
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                    
                                  
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="middlename" class="col-md-3 control-label">Middle Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="passwd" id="passwd" placeholder="Password">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="ccode" class="col-md-3 control-label">Country Code</label>
                                    <div class="col-md-9">
                                        <input type="number" size="3" class="form-control" name="ccode" id="ccode" placeholder="Country Code">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="phonenumber" class="col-md-3 control-label">Phone Number</label>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="phno" id="phno" placeholder="Phone Number">
                                    </div>
                                </div>
								<div class="form-group" style="margin-bottom:20px;">
								<label for="type" class="col-md-3 control-label">Account Type</label>
								<div class="col-md-9">
									<label class="radio-inline">
										<input type="radio" name="optradio" id="user" value="user">User
									</label>
									<label class="radio-inline">
										<input type="radio" name="optradio" id="ngo" value="ngo">NGO
									</label>
								</div>
								</div>
                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" name="submit" class="btn btn-info"><i class="icon-hand-right"></i> Sign Up</button>
                                        <!--<span style="margin-left:8px;">or</span>-->  
                                    </div>
                                </div>
                                
                                <!-- <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">
                                    
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                                    </div>                                          
                                        
                                </div> -->
                                
                                
                                
                            </form>
                         </div>
                    </div>

               
               
                
         </div> 
    </div>
	
	</body>
</html>
    