<?php 

	session_start();
	include("config.php");
	
	if($_SESSION["user_type"] != 'admin'){
		if($_SESSION["user_type"] == 'ngo'){
			//header('location:collrep.php');
		}
		else if($_SESSION["user_type"] == 'admin'){
			header('location:admin.php');
		}
		else{
			header('location:loginhome.php');
		}
	}
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	if(isset($_GET["action"]))
	{
		if($_GET["action"] == "approve")
		{
			$userid = $_GET["aid"];
			
				$sql = "UPDATE ngo_profile SET ngo_status = 'ACTIVE' WHERE user_profile_id = '$userid'";
				if($conn->query($sql)==true)
				{
					echo '<script>window.location="http://localhost/ngo/admin.php"</script>';
				}
				else
				{
					//echo '<script>alert("Error Deleting Quantity")</script>';
					//echo '<script>window.location="http://localhost/Cart/cart.php"</script>';
				}
				
		}
		if($_GET["action"] == "reject")
		{
			$userid = $_GET["rid"];
			$sql = "UPDATE ngo_profile SET ngo_status = 'INACTIVE' WHERE user_profile_id = '$userid'";
				if($conn->query($sql)==true)
				{
					echo '<script>window.location="http://localhost/ngo/admin.php"</script>';
				}
				else
				{
					//echo '<script>alert("Error Deleting Quantity")</script>';
					//echo '<script>window.location="http://localhost/Cart/cart.php"</script>';
				}
				
		}
	}
	
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">NGO Management System</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#browse">Browse NGOs</a>
            </li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#pending">Pending Requests</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#add">Add NGOs</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Signout.php">Signout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>NGO FINDER</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Find a deserving NGO and make a difference</p>
            <!--<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>-->
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search NGO" />
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="browse">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">List of NGOs</h2>
            <hr class="light my-4">
			
			<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Phone Number</th>
							<th>Email</th>
							<th>Website</th>
							<th>Contact Person</th>
							<th>NGO Status</th>
						</tr>
					</thead>
					<tbody>
					
					<?php 
						
							$sql = "SELECT * FROM ngo_profile";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								while($row = mysqli_fetch_assoc($result)){
					?>
									<tr>
									<td> <?php echo $row["name_ngo"]; ?> </td>
									<td> <?php echo $row["mobileno"]; ?> </td>
									<td> <?php echo $row["email"]; ?> </td>
									<td> <?php echo $row["website"]; ?> </td>
									<td> <?php echo $row["contactperson"]; ?> </td>
									<td> <?php echo $row["ngo_status"]; ?> </td>
								
									</tr>
						<?php
								}
							}
							
						?>
					
					</tbody>
			</table>
			
          </div>
        </div>
      </div>
    </section>

    <section id="pending">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Pending Approval</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
	  <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Website</th>
							<th>NGO Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody>
					
					<?php 
						
							$sql = "SELECT * FROM ngo_profile WHERE ngo_status = 'PENDING'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								while($row = mysqli_fetch_assoc($result)){
					?>
									<tr>
									<td> <?php echo $row["name_ngo"]; ?> </td>
									<td> <?php echo $row["email"]; ?> </td>
									<td> <?php echo $row["website"]; ?> </td>
									<td> <?php echo $row["ngo_status"]; ?> </td>
									<td><a href = "admin.php?action=approve&aid=<?php echo $row["user_profile_id"]; ?> "><span class="text-success">Approve</span></a></td>
									<td><a href = "admin.php?action=reject&rid=<?php echo $row["user_profile_id"]; ?> "><span class="text-danger">Reject</span></a></td>
									</tr>
						<?php
								}
							}
							
						?>
					
					</tbody>
				</table>
			</div>
		</div>
	</div>
     
    </section>

    <section class="bg-primary" id="add">
      <div class="container-fluid p-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center" style="padding:1px 2px">
					<h2 class="section-heading">ADD NGOs</h2>
					<hr class="my-4">
				</div>
			</div>
		</div>
      </div>
    </section>

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Bulk Upload</h2>
			
	<form method="POST" action="addngob.php" enctype="multipart/form-data">
	
	<div class="form-group">
		<div class="input-group input-file" name="Fichier1">
    		<input type="text" class="form-control" name="ngofile" id="ngofile" placeholder='Choose a file...' />			
            <span class="input-group-btn">
        		<button class="btn-sq btn-default btn-lg btn-choose" type="button">Choose</button>
    		</span>
		</div>
	</div>
	
	<div class="form-group">
		<button type="submit" class="btn-sq btn-primary btn-lg pull-right" name="submitb">Submit</button>
		<button type="reset" class="btn-sq btn-danger btn-lg pull-left">Reset</button>
	</div>
	</form>
		
      </div>
    </section>

    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="my-4">
            <p class="mb-5">Have any queries ? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
            <p>+91 9999999999</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
            <p>
              <a href="mailto:your-email@your-domain.com">juggernauts@chidhagni.com</a>
            </p>
          </div>
        </div><br><br>
		<div class="row">
		<div class="col-lg-10 mx-auto text-center" id="googleMap" style="height:300px;width:50%;">
	<script>
	function myMap() {
		var myCenter = new google.maps.LatLng(17.5159904, 78.3960801);
		var mapProp = {center:myCenter, zoom:12, scrollwheel:true, draggable:true, mapTypeId:google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
		var marker = new google.maps.Marker({position:myCenter});
		marker.setMap(map);
	}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAR0GTO_NC9UWAlHLuZwkICpKYW4JVRkdk&callback=myMap"></script>
	</div>
	</div>
      </div>
	  
	
	  <br><br>
		<footer class="container-fluid text-center">
			<a href="#page-top" title="To Top">
				<span class="glyphicon glyphicon-chevron-up"></span>
			</a>
			<p>Visit our website <a href="https://www.chidhagni.com" title="Visit Chidhagni">www.chidhagni.com</a></p>
		</footer>
    </section>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="vendor/uploadfile.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>
</html>
