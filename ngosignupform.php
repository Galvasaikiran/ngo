<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/jquery.js"></script>

	<script type="text/javascript">
           function Validateform(){
		      
			  var email = document.getElementById('email').value;
			  var fname = document.getElementById('fname').value;
			  var mname = document.getElementById('mname').value;
			  var lname = document.getElementById('lname').value;
			  var password = document.getElementById('passwd').value;
			  var countrycode = document.getElementById('ccode').value;
			  var phno = document.getElementById('phno').value;
			  var user = document.getElementById('user').checked;
			  var ngo = document.getElementById('ngo').checked;
			  

			  if(email == "" || email == "Email Address"){
			     alert("Email Not Entered");
				 return false;
			  }
			  else if(fname == "" || fname == "First Name"){
				alert("First Name Not Entered");
				return false;
			  }
			  else if(mname == "" || mname == "Middle Name"){
				alert("Middle Name Not Entered");
				return false;
			  }
			  else if(lname == "" || lname == "Last Name"){
				alert("Last Name Not Entered");
				return false;
			  }
			  else if(password == "" || password == "Password"){
			     alert("Password Not Entered");
				 return false;
			  }
			  else if(countrycode == "" || countrycode == "Country Code"){
			     alert("Country Code Not Entered");
				 return false;
			  }else if(phno == "" || phno == "Phone Number"){
			     alert("Phone Number Not Entered");
				 return false;
			  }  
			  else if(user == "" && ngo == ""){
				  alert("Please select account type");
			  }
			  else{
			     return true;
			  }
		   
		   }
		   
	</script>
  </head>
  <body>
  <br>
  
  <div class="container"> 
  <div id="signupbox" style="margin-top:30px" class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">NGO Registration Form</div>
                        </div>  
                        <div class="panel-body">
                            <form id="signupform" class="form-horizontal" role="form" method="POST" action="ngoregister.php" onsubmit="return Validateform()" enctype="multipart/form-data">
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>  
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Enter NGO Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="ngoname" id="ngoname" placeholder="NGO Name">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="city" class="col-md-3 control-label">City</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="city" id="city" placeholder="Middle Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="district" class="col-md-3 control-label">District</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="district" id="disrict" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-md-3 control-label">State</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="state" id="state" placeholder="State">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="pincode" class="col-md-3 control-label">Pin Code</label>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="pincode" id="pincode" placeholder="Pin Code">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="mbno" class="col-md-3 control-label">Mobile Number</label>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="mbno" id="mbno" placeholder="Mobile Number">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="phno" class="col-md-3 control-label">Phone Number</label>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="phno" id="phno" placeholder="Phone Number (Including Area Code - Optional)">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Official NGO Email">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="website" class="col-md-3 control-label">Website</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="website" id="website" placeholder="Official NGO Website (Optional)">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="contactp" class="col-md-3 control-label">Contact Person</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="contactp" id="contactp" placeholder="Contact Person Name">
                                    </div>
                                </div>
								<div class="form-group">
								<label class="col-md-3 control-label">Sector</label>
								<div class="col-md-4">
									<label class="radio-inline">
										<input type="checkbox" name="sector" id="sector[]" value="health">Health
									</label>
									<label class="radio-inline">
										<input type="checkbox" name="sector" id="sector[]" value="education">Education
									</label>
								</div>
								</div>
								<div class="form-group">
                                    <label for="contactp" class="col-md-3 control-label">Aim/Objective</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="aim" id="aim" placeholder="Objective">
                                    </div>
                                </div>
								<div class="form-group">
									<label for="file" class="col-md-3 control-label">Registration Certificate :</label><br>
										<!--<form action="ngoform.php" method="post" enctype="multipart/form-data">-->					
										<input class="col-md-6 control-label" type="file"  id="file" name="file" required>
								</div>
                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" name="submit" class="btn btn-info"><i class="icon-hand-right"></i> Submit</button>   
                                    </div>
                                </div>
                                </div>
                            </form>
                         
                    </div>        
         </div> 
    </div>
	</div>
	
	</body>
</html>
  
  