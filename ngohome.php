<?php 

	session_start();
	include("config.php");
	
	if($_SESSION["user_type"] != 'admin'){
		if($_SESSION["user_type"] == 'ngo'){
			//header('location:collrep.php');
		}
		else if($_SESSION["user_type"] == 'admin'){
			header('location:admin.php');
		}
		else{
			header('location:loginhome.php');
		}
	}
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NGO DASHBOARD</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.css" rel="stylesheet">
	  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="jquery.paginate.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="script/validation.min.js"></script>
<script type="text/javascript" src="script/login.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">



	<style>
	.a{
    width: 50%;
    padding: 8px ;
    margin: 8px 0;
    box-sizing: border-box;
    border: 2px solid white;
    border-radius: 4px;
}
.btn{
	background-color: #008CBA;
    border: none;
    color: white;
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
	border-radius: 1px
}

	
	</style>

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">NGO Management System</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#uploaddata">UPLOAD DATA</a>
            </li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#addproducts">ADD PRODUCTS</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#addprojects">ADD PROJECTS</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#postjobs">POST JOBS</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#viewdata">VIEW DATA</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">CONTACT</a>
			</li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Signout.php">Signout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>NGO FINDER</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Find a deserving NGO and make a difference</p>
            <!--<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>-->
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search NGO" />
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="uploaddata">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">UPLOAD DATA</h2>



<div class="container ">
	<h2>enter the data as in the template</h2>	
    <?php if(!empty($import_status_message)){
        echo '<div class="alert '.$message_stauts_class.'">'.$import_status_message.'</div>';
    } ?>
    <div class="panel panel-default " >        
        <div class="panel-body ">
			<br>
			<div class="row">
				<form action="import.php" method="post" enctype="multipart/form-data" id="import_form">				
						<div class="col-md-4 ">
						<input type="file" name="file" />						
						<input type="submit" class="btn btn-primary" name="import_data" value="import_data">
						 &nbsp; &nbsp; 
						 
						<a  style="color:white;" href="ngodata.xlsx"	download="Template">data template</a>
						</div>
				</form>
			</div>
			<br>
        </div>
    </div>	
	
</div>
<div class="container">
	<h2>enter the financialdetails</h2>	
    <?php if(!empty($import_status_message1)){
        echo '<div class="alert '.$message_stauts_class1.'">'.$import_status_message1.'</div>';
    } ?>
    <div class="panel panel-default ">        
        <div class="panel-body">
			<br>
			<div class="row">
				<form action="import1.php" method="post" enctype="multipart/form-data" id="import_form">				
						<div class="col-md-3">
						<input type="file" name="file" />
						 &nbsp; &nbsp; 
						<input type="submit" class="btn btn-primary" name="import_data" value="import_data">
						
						<a style="color:white;"href="ngofinancials.xlsx"	download="Template">Financial data Template</a>
				</form>
			</div>
			<br>
        </div>
    </div>	
	
</div>

			
          </div>
        </div>
      </div>
    </section>

    <section id="addproducts">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">ADD PRODUCTS</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
<div class="container"  style="border-radius: 5px;
    background-color: #f2f2f2;
    padding: 40px;">
 
  <form  action="ngoproduct.php" method="post" enctype="multipart/form-data" ">
	<div class="form-group">
	<label>product name :</label><br>
	<input type="text" id="product" name="product" placeholder="product name" class="a" required ><br>
	<label>product price* :</label><br>
	<input type="number" id="price" name="price" placeholder="enter price" class="a" required><br>
	<label>year* :</label><br>
	<input type="year" id="year" name="year" placeholder="enter year" class="a" required><br>
	
	
	<label>about the product* :</label><br>
	<textarea id="desc" name="desc" placeholder="enter complete info" rows="3" class="a" required></textarea><br>
	<label>upload the product image</label>&nbsp;&nbsp;
	
	 <input type="file" name="image1" /><br>
	
	
	<input type="submit" name="submit" id="submit" value="add" class="a" />
  </form>
</div>

    </section>

    <section class="bg-primary" id="addprojects">
      <div class="container-fluid p-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center" >
					<h2 class="section-heading">ADD PROJECTS</h2>
					<hr class="my-4">
				</div>
			</div>
		</div>
		
		
		


<div class="container">
 
  <form  action="ngoproject.php" method="post" enctype="multipart/form-data">
	<div class="form-group">
	<label>event name :</label><br>
	<input type="text" id="ename" name="ename" placeholder="enter event name" class="a" required><br>
	<label>event date* :</label><br>
	<input type="date" id="date" name="date" placeholder="enter the date" class="a" required><br>
	<label>event time* :</label><br>
	<input type="time" id="time" name="time" placeholder="enter time" class="a" required><br>
	<label>event place* :</label><br>
	<input type="text" id="place" name="place" placeholder="enter text" class="a" required><br>
	
	
	<label>about the project** :</label><br>
	<textarea id="desc" name="desc" placeholder="enter complete info" rows="3" class="a" required></textarea><br>
	<label>upload the product image</label>
	 <input type="file" name="image1" />
	
	<input type="submit" name="submit" id="submit" value="add" class="a" />
	
  </form>
</div>
		

      </div>
    </section>

	
	<section id="postjobs">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">POST JOBS</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>

<div class="container" style="border-radius: 5px;
    background-color: #f2f2f2;
    padding: 40px;">
  
  <form  action="ngojob.php" method="post">
	<div class="form-group">
	<label>Job position :</label><br>
	<input type="text" id="position" name="position" placeholder="enter job position" class="a" required><br>
	<label>job description* :</label><br>
	<input type="text" id="desc" name="desc" placeholder="enter info" class="a" required><br>
	<label>duration* :</label><br>
	<input type="number" id="duration" name="duration" placeholder="enter duration" class="a" required><br>
	<label>Location* :</label><br>
	<input type="text" id="location" name="location" placeholder="enter location" class="a" required><br>
	<label>job type *:</label><br>
	<select name="type">
	<option>FULL TIME</option>
	<option>PART TIME<option>
	
	</select><br>
	
	<input type="submit" name="submit" id="submit" value="add" class="a">
	
  </form>
</div>

    </section>

	
	
	    <section class="bg-primary" id="viewdata">
      <div class="container-fluid p-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center" >
					<h2 class="section-heading">VIEW DATA</h2>
					<hr class="my-4">
				</div>
			</div>
		</div>
		
		
		<?php

 $userid=$_SESSION['user_id'];
 
 $sql="select * from ngoproducts where user_profile_id='$userid'";
 $sql1="select * from ngoevents where user_profile_id='$userid'";
  $sql2="select * from ngojobs where user_profile_id='$userid'";
 $result=mysqli_query($conn,$sql);
 $result1=mysqli_query($conn,$sql1);
 $result2=mysqli_query($conn,$sql2);
?>


	        <div class="container">
        <h2 align="center"> PRODUCT DETAILS</h2> 
<div id="prdct">		
  <table class="table">
    <thead>
      <tr>
        <th>productname</th>
        <th>productprice</th>
        <th>year</th>
		<th>about</th>
		<th>image</th>
		<th><button type='submit' name='save1' value="DELETE">DELETE</button></th>
      </tr>
    </thead>
	<tbody>
	<form method="post" action="deletecoll.php" >
	<?php while($row=mysqli_fetch_array($result)){?>
    <?php echo '
      <tr class="info">
        <td> '.$row["product_name"].'</td>
        <td> '.$row["product_price"].'</td>
		<td> '.$row["year"].' </td>
		<td> '.$row["about"].' </td>
		<td><img src="images/'.$row["image"].'" width="50px" height="50px" /></td>
		<td><input type="checkbox" id="checkItem" name="check[]" value="'.$row["product_name"].'"></td>
      </tr>   '   ?>
      
	<?php } ?>
       <?php echo "</tbody>";
    echo "</table>";
		echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('#prdct').paginate({
        'elemsPerPage': 2,
            'maxButtons': 6
    });
});
</script>";?>
</form>
<?php
echo '<script>
$("#checAl").click(function)() {
	$("input:checkbox").not(this).prop("checked",this.checked);
});
</script>';?>
  </div>
  
  <h2 align="center"> PROJECTS DETAILS</h2>    
<div id="prjct">  
  <table class="table">
    <thead>
      <tr>
        <th>eventname</th>
        <th>eventdate</th>
        <th>eventtime</th>
		<th>about</th>
		<th>image</th>
      </tr>
    </thead>
	<tbody>
	<?php while($row=mysqli_fetch_array($result1)){?>
    <?php echo'
      <tr class="info">
        <td>'. $row["event_name"] .'</td>
        <td>'.$row["event_date"].' </td>
		<td>'.$row["event_time"].'</td>
		<td>'.$row["about"].'</td>
		<td><img src="images/'.$row["image"].'" width="50px" height="50px" /></td>
      </tr>     '?> 
      
	<?php } ?>
	<?php echo "</tbody>";
    echo "</table>";
		echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('#prjct').paginate({
        'elemsPerPage': 2,
            'maxButtons': 6
    });
});
</script>";?>
  </div>
  
  <h2 align="center"> JOB DETAILS</h2>   
  <div id="jb">
    <table class="table">
    <thead>
      <tr>
        <th>jobposition</th>
        <th>jobdescription</th>
        <th>duration</th>
		<th>location</th>
		<th>jobtype</th>
      </tr>
    </thead>
	<tbody>
	<?php while($row=mysqli_fetch_array($result2)){?>
    <?php echo '
      <tr class="info">
        <td>'.$row["job_description"].'</td>
        <td>'.$row["job_position"].'</td>
		<td>'.$row["duration"].'</td>
		<td>'.$row["job_location"].'</td>
		<td>'.$row["job_type"].'</td>
      </tr>'?>      
      
	<?php } ?>
    <?php echo "</tbody>";
    echo "</table>";
		echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('#jb').paginate({
        'elemsPerPage': 2,
            'maxButtons': 6
    });
});
</script>";?>
  
  
  </div>
</div>

      </div>
    </section>

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="my-4">
            <p class="mb-5">Have any queries ? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
            <p>+91 9999999999</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
            <p>
              <a href="mailto:your-email@your-domain.com">juggernauts@chidhagni.com</a>
            </p>
          </div>
        </div><br><br>
		<div class="row">
		<div class="col-lg-10 mx-auto text-center" id="googleMap" style="height:300px;width:50%;">
	<script>
	function myMap() {
		var myCenter = new google.maps.LatLng(17.5159904, 78.3960801);
		var mapProp = {center:myCenter, zoom:12, scrollwheel:true, draggable:true, mapTypeId:google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
		var marker = new google.maps.Marker({position:myCenter});
		marker.setMap(map);
	}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAR0GTO_NC9UWAlHLuZwkICpKYW4JVRkdk&callback=myMap"></script>
	</div>
	</div>
      </div>
	  
	
	  <br><br>
		<footer class="container-fluid text-center">
			<a href="#page-top" title="To Top">
				<span class="glyphicon glyphicon-chevron-up"></span>
			</a>
			<p>Visit our website <a href="https://www.chidhagni.com" title="Visit Chidhagni">www.chidhagni.com</a></p>
		</footer>
    </section>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="vendor/uploadfile.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>
</html>
