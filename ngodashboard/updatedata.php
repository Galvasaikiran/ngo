<?php

session_start();

?>
<html lang="en">
<head>
 
  <title>ngo dashboard</title>
<style>  .a{
    width: 50%;
    padding: 8px ;
    margin: 8px 0;
    box-sizing: border-box;
    border: 2px solid blue;
    border-radius: 4px;
}
.btn{
	background-color: #008CBA;
    border: none;
    color: white;
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
	border-radius: 1px
}

</style>
  
</head>
<body>
<?php 
  $conn=mysqli_connect("localhost","root","","ngo");
if(!empty($_GET['import_status'])) {
    switch($_GET['import_status']) {
        case 'success':
            $message_stauts_class = 'alert-success';
            $import_status_message = ' cin data inserted successfully.';
            break;
        case 'error':
            $message_stauts_class = 'alert-danger';
            $import_status_message = 'Error: Please try again.';
            break;
        case 'invalid_file':
            $message_stauts_class = 'alert-danger';
            $import_status_message = 'Error: Please upload a valid CSV file.';
            break;
        default:
            $message_stauts_class = '';
            $import_status_message = '';
    }
}
if(!empty($_GET['email_status'])) {
    switch($_GET['email_status']) {
        case 'success':
            $message_stauts_class1 = 'alert-success';
            $import_status_message1 = ' mail sent successfully.';
            break;
        case 'error':
            $message_stauts_class1 = 'alert-danger';
            $import_status_message1 = 'Error: Please try again.';
            break;
        default:
            $message_stauts_class1 = '';
            $import_status_message1 = '';
    }
}
if(!empty($_GET['dic_status'])) {
    switch($_GET['dic_status']) {
        case 'success':
            $message_stauts_class2 = 'alert-success';
            $import_status_message2 = ' director data inserted successfully';
            break;
        case 'error':
            $message_stauts_class2 = 'alert-danger';
            $import_status_message2 = 'Error: Please try again.';
            break;
        default:
            $message_stauts_class2 = '';
            $import_status_message2 = '';
    }
}
?>
<title>Update The Data</title>
<script type="text/javascript" src="script/validation.min.js"></script>
<script type="text/javascript" src="script/login.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
<div class="container ">
	<h2>enter the data as in the template</h2>	
    <?php if(!empty($import_status_message)){
        echo '<div class="alert '.$message_stauts_class.'">'.$import_status_message.'</div>';
    } ?>
    <div class="panel panel-default a" >        
        <div class="panel-body ">
			<br>
			<div class="row">
				<form action="import.php" method="post" enctype="multipart/form-data" id="import_form">				
						<div class="col-md-4 ">
						<input type="file" name="file" />						
						<input type="submit" class="btn btn-primary" name="import_data" value="import_data">
						 &nbsp; &nbsp; 
						<a  href="ngodata.xlsx"	download="Template">Company CIN Template</a>
						</div>
				</form>
			</div>
			<br>
        </div>
    </div>	
	<div>
	<div class="row">
				<form action="mail.php" method="post" enctype="multipart/form-data" id="import_form">				
						<div class="col-md-5">
						<input type="submit" class="btn btn-primary" name="mail" value="send mail">
						</div>			
				</form>
			</div>
	</div>
</div>
<div class="container">
	<h2>enter the financialdetails</h2>	
    <?php if(!empty($import_status_message1)){
        echo '<div class="alert '.$message_stauts_class1.'">'.$import_status_message1.'</div>';
    } ?>
    <div class="panel panel-default a">        
        <div class="panel-body">
			<br>
			<div class="row">
				<form action="import1.php" method="post" enctype="multipart/form-data" id="import_form">				
						<div class="col-md-3">
						<input type="file" name="file" />
						 &nbsp; &nbsp; 
						<input type="submit" class="btn btn-primary" name="import_data" value="import_data">
						
						<a href="ngofinancials.xlsx"	download="Template">Directors Template</a>
				</form>
			</div>
			<br>
        </div>
    </div>	
	<div>
	<div class="row">
	<?php if(!empty($import_status_message2)){
        echo '<div class="alert '.$message_stauts_class2.'">'.$import_status_message2.'</div>';
    } ?>
				<form action="mail.php" method="post" enctype="multipart/form-data" id="import_form">				
						<div class="col-md-5">
						<input type="submit" class="btn btn-primary" name="mail" value="send mail">
						</div>			
				</form>
			</div>
	</div>
</div>

</body>
</html>