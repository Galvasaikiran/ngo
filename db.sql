-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.29-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ngo
CREATE DATABASE IF NOT EXISTS `ngo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ngo`;

-- Dumping structure for table ngo.ngo_data
CREATE TABLE IF NOT EXISTS `ngo_data` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `year_estd` varchar(50) DEFAULT NULL,
  `financial_score` int(11) DEFAULT NULL,
  `description` longtext,
  `mission` longtext,
  `vision` longtext,
  `trustee1` varchar(50) DEFAULT NULL,
  `trustee2` varchar(50) DEFAULT NULL,
  `trustee3` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `ngo_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ngo_data` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_events
CREATE TABLE IF NOT EXISTS `ngo_events` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `event_id` varchar(50) DEFAULT NULL,
  `event_name` varchar(50) DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `event_place` varchar(50) DEFAULT NULL,
  `about` longtext,
  `image` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_events: ~0 rows (approximately)
/*!40000 ALTER TABLE `ngo_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ngo_events` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_newsletter
CREATE TABLE IF NOT EXISTS `ngo_newsletter` (
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_newsletter: ~2 rows (approximately)
/*!40000 ALTER TABLE `ngo_newsletter` DISABLE KEYS */;
INSERT INTO `ngo_newsletter` (`email`) VALUES
	('areeb.uddin@chidhagni.com'),
	('areeb@gmail.com');
/*!40000 ALTER TABLE `ngo_newsletter` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_products
CREATE TABLE IF NOT EXISTS `ngo_products` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `product_id` varchar(50) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `about` longtext,
  `image` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_products: ~0 rows (approximately)
/*!40000 ALTER TABLE `ngo_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `ngo_products` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_profile
CREATE TABLE IF NOT EXISTS `ngo_profile` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `name_ngo` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `mobileno` bigint(20) DEFAULT NULL,
  `phoneno` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `contactperson` varchar(50) DEFAULT NULL,
  `sector` varchar(50) DEFAULT NULL,
  `aim` varchar(100) DEFAULT NULL,
  `registration_certificate` varchar(100) DEFAULT NULL,
  `ngo_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_profile: ~1 rows (approximately)
/*!40000 ALTER TABLE `ngo_profile` DISABLE KEYS */;
INSERT INTO `ngo_profile` (`user_profile_id`, `name_ngo`, `address`, `city`, `district`, `state`, `pincode`, `mobileno`, `phoneno`, `email`, `website`, `contactperson`, `sector`, `aim`, `registration_certificate`, `ngo_status`) VALUES
	('2018MNxsQW', 'H M Charitable trust', '42, navketan Industrial Estate', 'Mumbai', NULL, 'Maharashtra', 400093, 66756001, NULL, 'hmtrust@hmgroupindia.com', 'www.hmgroupindia.com', 'Haresh', 'Education', 'gbbj hbbhbj jhbbbju jbhbb ybbyu tfyfy huhuh tfrft yguyu tyfyfy', NULL, 'ACTIVE');
/*!40000 ALTER TABLE `ngo_profile` ENABLE KEYS */;

-- Dumping structure for table ngo.user_account
CREATE TABLE IF NOT EXISTS `user_account` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `pswd` varchar(256) DEFAULT NULL,
  `pswd_salt` varchar(50) DEFAULT NULL,
  `pswd_hash_fun` varchar(50) DEFAULT NULL,
  `user_account_status_id` int(11) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `user_status` varchar(50) DEFAULT NULL,
  `newsletter` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.user_account: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` (`user_profile_id`, `email`, `pswd`, `pswd_salt`, `pswd_hash_fun`, `user_account_status_id`, `user_type`, `user_status`, `newsletter`) VALUES
	('20187565fb8e00', 'areeb.uddin@chidhagni.com', '0badf1e63419d7056f53df2534821c261be78733b5eaebdfdb096a4c08d85735', '7f7a0b162e1f47098b39', 'sha256', 1, 'admin', NULL, 'N');
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;

-- Dumping structure for table ngo.user_account_status
CREATE TABLE IF NOT EXISTS `user_account_status` (
  `account_status_id` int(11) DEFAULT NULL,
  `account_status_code` varchar(50) DEFAULT NULL,
  `account_status_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.user_account_status: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_account_status` DISABLE KEYS */;
INSERT INTO `user_account_status` (`account_status_id`, `account_status_code`, `account_status_name`) VALUES
	(1, 'ACT', 'Activated');
/*!40000 ALTER TABLE `user_account_status` ENABLE KEYS */;

-- Dumping structure for table ngo.user_profile
CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.user_profile: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` (`user_profile_id`, `first_name`, `middle_name`, `last_name`, `email`, `country_code`, `phone`) VALUES
	('20187565fb8e00', 'Areeb', 'uddin', 'Aatif', 'areeb.uddin@chidhagni.com', 91, 9502148597);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
